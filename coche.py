class Coche:
    def __init__(self, c, con, mar, mod, mat, vel = 0):
        self.color = c
        self.conductor = con
        self.marca = mar
        self.modelo = mod
        self.matricula = mat
        self.velocidad = vel
    def acelera(self, crecimiento):
        self.velocidad += crecimiento
        
    def frena(self, decrecimiento):
        self.velocidad += decrecimiento
    

coche01 = Coche("Rojo", "Isabel", "Toyota", "Yaris", "6786SDC", 20)
coche02 = Coche("Blanco", "Fernando", "Seat", "Ibiza", "1234NHB", 30)



#he editado esto
aceleracion_coche01 = coche01.acelera(10)
frenada_coche02 = coche02.frena(-8)
print(coche01.velocidad) #aquí estoy probando si sale el resultado esperado
print(coche02.velocidad) #aquí también

