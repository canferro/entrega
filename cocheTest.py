import unittest

from coche import Coche

class TestCoche(unittest.TestCase):
    def test_acelera01(self):
        c1 = Coche("Rojo", "Isabel", "Toyota", "Yaris", "6786SDC", 20)
        c2 = Coche("Blanco", "Fernando", "Seat", "Ibiza", "1234NHB", 30)

        acelera = c1.acelera(10)
        self.assertEqual(c1.velocidad, 30)
    def test_acelera02(self):
        c1 = Coche("Rojo", "Isabel", "Toyota", "Yaris", "6786SDC", 20)
        c2 = Coche("Blanco", "Fernando", "Seat", "Ibiza", "1234NHB", 30)

        acelera = c1.acelera(30)
        self.assertEqual(c1.velocidad, 50)
    def test_frena01(self):
        c1 = Coche("Rojo", "Isabel", "Toyota", "Yaris", "6786SDC", 20)
        c2 = Coche("Blanco", "Fernando", "Seat", "Ibiza", "1234NHB", 30)

        frena = c2.frena(-8)
        self.assertEqual(c2.velocidad, 22)
    def test_frena02(self):
        c1 = Coche("Rojo", "Isabel", "Toyota", "Yaris", "6786SDC", 20)
        c2 = Coche("Blanco", "Fernando", "Seat", "Ibiza", "1234NHB", 30)

        frena = c2.frena(-20)
        self.assertEqual(c2.velocidad, 10)


unittest.main()